# Mi-Casa Su-Casa

Mi-Casa is an attack that exploits the fact that many devices are configured
through web interfaces accessed by entering an internal IP address in a
browser's URL bar.

## Primary Assumption

If you're connected to your
own network and browse to 192.168.1.1. Then connect to my network and visit the 
same IP address:

Instructions to follow