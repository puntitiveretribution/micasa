# -*- coding: utf-8 -*-
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  Author: Elliott Thompson

import pyric                    # pyric errors
import pyric.pyw as pyw         # iw functionality
from termcolor import colored   # colors
from NetworkDevice import NetworkDevice

class Core():

    def check_interfaces(self):
        
        print(colored('Checking Wireless Interfaces..', 'green'))
        network_interface_names = pyw.interfaces()
        network_devices = []
        for network_interface_name in network_interface_names:
            network_device = NetworkDevice(network_interface_name)
            network_devices.append(network_device)           
        
        can_continue = True
        problems = []
        # What questions need answering?

        # - Are there any wireless devices
        if [network_device for network_device in network_devices if network_device.media_type == 'wireless']:
            print(colored('\t✔️ Wireless device found','green'))
        else:
            print(colored('\tX Wireless device not found', 'red'))
            can_continue = False

        # - Can we attack 2GHz
        if [network_device for network_device in network_devices if '2GHz' in network_device.wireless_bands]:
            print(colored('\t✔️ 2GHz device found','green'))
        else:
            print(colored('\tX 2GHz device not found', 'red'))

        # - Can we attack 5GHz
        if [network_device for network_device in network_devices if '5GHz' in network_device.wireless_bands]:
            print(colored('\t✔️ 5GHz device found','green'))
        else:
            print(colored('\tX 5GHz device not found', 'yellow'))
            problems.append('5GHz networks can\'t be targeted')

        # - Do we have any monitor-mode interfaces
        if [network_device for network_device in network_devices if 'monitor' in network_device.wireless_modes]:
            print(colored('\t✔️ Monitoring device found','green'))
        else:
            print(colored('\tX Monitoring device not found', 'yellow'))
            problems.append('Without a monitoring interface, target data needs to be entered manually')
        
        # - Do we have any AP-mode interfaces
        if [network_device for network_device in network_devices if 'AP' in network_device.wireless_modes]:
            print(colored('\t✔️ Access Point device found','green'))
        else:
            print(colored('\tX Access Point device not found', 'red'))
            can_continue = False

        # - Do we have any injection interfaces
        if [network_device for network_device in network_devices if network_device.can_inject == True]:
            print(colored('\t✔️ Injection capable device found','green'))
        else:
            print(colored('\tX Injection capable device not found', 'red'))
            can_continue = False

        # - Do we have any internet-connected interfaces
        if [network_device for network_device in network_devices if network_device.is_connected == True]:
            print(colored('\t✔️ Internet connected device found','green'))
        else:
            print(colored('\tX Internet connected device not found', 'red'))
            can_continue = False

        can_continue = True

        if can_continue:
            if problems:
                print(colored('\nThe following ' + str(len(problems)) + ' problems were identified...', 'yellow'))

                for problem in problems:
                    print(colored('\t' + problem, 'yellow'))

                print('\nDo you wish to continue?')
            else:
                print('No problems')
                # Interface to provide hostapd (needs AP & injection)* - No others [1]
                # Interface to provide monitoring (needs monitoring) - Share [2]
                # Interface to provide deauth (needs injection) - Share [2]
                # Interface to provide internet access (just requires internet)* - No others [3]
                # If there's more than one option, just pick one & ask to change.
        else:
            print(colored('Unable to continue, mandatory condition failed', 'red'))


# Test
test = Core
test.check_interfaces(test)